﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour {

    // Using same speed reference in both, desktop and other devices
    public float speed = 1000;
    public float force = 11.0f; //used for accelerometer controls
    public Text countText, winText;

    private Rigidbody rb;

    private int count;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        setCountText();
        winText.text = "";
    }

    void Main()
    {
        // Preventing mobile devices going in to sleep mode
        //(actual problem if only accelerometer input is used)
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    void Update()
    {

        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            // Exit condition for Desktop devices
            if (Input.GetKey("escape"))
                Application.Quit();
        }
        else
        {
            // Exit condition for mobile devices
            if (Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();
        }
    }

        void FixedUpdate()
    {
        //are we using a computer as opposed to a mobile device with an accelerometer?
        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            //start of movement code from rollerball tutorial
            //used for desktop movement
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

            rb.AddForce(movement * speed);
            //end of movement code from rollerball tutorial
        }
        //or are we on mobile with an accelerometer?
        else
        {
            // Player movement in mobile devices
            // Building of force vector 
            Vector3 movement = new Vector3(Input.acceleration.x, 0.0f, Input.acceleration.y);
            // Adding force to rigidbody
            rb.AddForce(movement * speed);
        }
    }

    void OnTriggerEnter(Collider other)
    { 
        if (other.gameObject.CompareTag("Pick Up") || other.gameObject.CompareTag("Snowman"))
        {
            other.gameObject.SetActive(false);
            count++;
            setCountText();
        }
    }

    void setCountText()
    {
        countText.text = "Count " + count.ToString();
        if(count >= 22)
        {
            winText.text = "You Won !!";
        }
    }
}
